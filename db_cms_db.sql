-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: cmsdatabase
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `coursedetails`
--

DROP TABLE IF EXISTS `coursedetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coursedetails` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `coursename` varchar(255) NOT NULL DEFAULT '',
  `courseid` varchar(255) NOT NULL DEFAULT '',
  `noofyears` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coursedetails`
--

LOCK TABLES `coursedetails` WRITE;
/*!40000 ALTER TABLE `coursedetails` DISABLE KEYS */;
INSERT INTO `coursedetails` VALUES (1,'B.Tech','91a','4'),(2,'M.Tech','81a','2');
/*!40000 ALTER TABLE `coursedetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dailyattendance`
--

DROP TABLE IF EXISTS `dailyattendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dailyattendance` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `studentid` varchar(255) NOT NULL DEFAULT '',
  `subjectid` varchar(255) NOT NULL DEFAULT '',
  `date` varchar(255) NOT NULL DEFAULT '',
  `period` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dailyattendance`
--

LOCK TABLES `dailyattendance` WRITE;
/*!40000 ALTER TABLE `dailyattendance` DISABLE KEYS */;
INSERT INTO `dailyattendance` VALUES (47,'123','23','2016-12-14','5'),(48,'123','23','2016-12-14','6'),(49,'123','23','2016-12-14','7'),(44,'123','23','2016-12-14','1'),(45,'123','23','2016-12-14','3'),(46,'123','23','2016-12-14','4'),(28,'123','23','2016-12-12','2'),(33,'123','23','2017-01-12','1'),(30,'123','23','2016-12-12','2'),(31,'123','23','2016-12-12','2'),(32,'123','23','2016-12-12','2'),(34,'123','23','2017-01-12','2'),(35,'123','23','2017-01-12','6'),(36,'123','23','2017-01-12','7'),(37,'123','23','2017-01-12','1'),(38,'123','23','2017-01-12','2'),(39,'123','23','2017-01-12','6'),(40,'123','23','2017-01-12','7'),(41,'123','23','2016-12-17','1'),(42,'123','23','2016-04-12','1'),(43,'123','23','2016-04-12','2');
/*!40000 ALTER TABLE `dailyattendance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departmentdetails`
--

DROP TABLE IF EXISTS `departmentdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departmentdetails` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `coursename` varchar(255) NOT NULL DEFAULT '',
  `departmentname` varchar(255) NOT NULL DEFAULT '',
  `departmentid` varchar(255) NOT NULL DEFAULT '',
  `yearno` varchar(255) NOT NULL DEFAULT '',
  `noofsections` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departmentdetails`
--

LOCK TABLES `departmentdetails` WRITE;
/*!40000 ALTER TABLE `departmentdetails` DISABLE KEYS */;
INSERT INTO `departmentdetails` VALUES (1,'Combinatorial Algorithms','CSE','2342','2012','3'),(2,'Basic Civil Eng.','CIVIL','98','2016','3');
/*!40000 ALTER TABLE `departmentdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `examresults`
--

DROP TABLE IF EXISTS `examresults`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `examresults` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `subjectid` varchar(255) NOT NULL,
  `studentid` varchar(255) NOT NULL,
  `internalmarks` int(2) NOT NULL,
  `externalmarks` int(2) NOT NULL,
  `marksrequired` int(2) NOT NULL,
  `total` int(3) NOT NULL,
  `credits` int(1) NOT NULL,
  `result` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `examresults`
--

LOCK TABLES `examresults` WRITE;
/*!40000 ALTER TABLE `examresults` DISABLE KEYS */;
/*!40000 ALTER TABLE `examresults` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historicdata`
--

DROP TABLE IF EXISTS `historicdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historicdata` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `studentid` varchar(255) NOT NULL DEFAULT '',
  `year` varchar(255) NOT NULL DEFAULT '',
  `semester` varchar(255) NOT NULL DEFAULT '',
  `percentage` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historicdata`
--

LOCK TABLES `historicdata` WRITE;
/*!40000 ALTER TABLE `historicdata` DISABLE KEYS */;
/*!40000 ALTER TABLE `historicdata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logindetails`
--

DROP TABLE IF EXISTS `logindetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logindetails` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `permission` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logindetails`
--

LOCK TABLES `logindetails` WRITE;
/*!40000 ALTER TABLE `logindetails` DISABLE KEYS */;
INSERT INTO `logindetails` VALUES (1,'amith','amith','1'),(2,'basaiah','basaiah2','1'),(3,'chandana','chandana','1'),(5,'nijay','nijay','2'),(6,'harshit','harshit','3'),(7,'ajay','ajay','3'),(8,'hash','hash','2'),(9,'Sanjay Kumar','sanjay','2');
/*!40000 ALTER TABLE `logindetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perioddetails`
--

DROP TABLE IF EXISTS `perioddetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perioddetails` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `date` varchar(255) NOT NULL DEFAULT '',
  `period` varchar(255) NOT NULL DEFAULT '',
  `subjectcode` varchar(255) NOT NULL DEFAULT '',
  `staffid` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perioddetails`
--

LOCK TABLES `perioddetails` WRITE;
/*!40000 ALTER TABLE `perioddetails` DISABLE KEYS */;
INSERT INTO `perioddetails` VALUES (1,'2016-12-12','2','23','Sanjay Kumar'),(2,'2017-01-12','1','23','Sanjay Kumar'),(3,'2017-01-12','2','23','Sanjay Kumar'),(4,'2017-01-12','6','23','Sanjay Kumar'),(5,'2017-01-12','7','23','Sanjay Kumar'),(6,'2017-01-12','1','23','Sanjay Kumar'),(7,'2017-01-12','2','23','Sanjay Kumar'),(8,'2017-01-12','6','23','Sanjay Kumar'),(9,'2017-01-12','7','23','Sanjay Kumar'),(10,'2016-12-17','1','23','Sanjay Kumar'),(11,'2016-04-12','1','23','Sanjay Kumar'),(12,'2016-04-12','2','23','Sanjay Kumar'),(13,'2016-12-14','1','23','Sanjay Kumar'),(14,'2016-12-14','3','23','Sanjay Kumar'),(15,'2016-12-14','4','23','Sanjay Kumar'),(16,'2016-12-14','5','23','Sanjay Kumar'),(17,'2016-12-14','6','23','Sanjay Kumar'),(18,'2016-12-14','7','23','Sanjay Kumar');
/*!40000 ALTER TABLE `perioddetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staffdetails`
--

DROP TABLE IF EXISTS `staffdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staffdetails` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `staffname` varchar(255) NOT NULL DEFAULT '',
  `staffid` varchar(255) NOT NULL DEFAULT '',
  `mobileno` varchar(255) NOT NULL DEFAULT '',
  `emailid` varchar(255) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  `qualification` varchar(255) NOT NULL DEFAULT '',
  `designation` varchar(255) NOT NULL DEFAULT '',
  `experience` varchar(255) NOT NULL DEFAULT '',
  `imagepath` varchar(255) NOT NULL DEFAULT '',
  `doj` varchar(255) NOT NULL DEFAULT '',
  `department` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3434 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staffdetails`
--

LOCK TABLES `staffdetails` WRITE;
/*!40000 ALTER TABLE `staffdetails` DISABLE KEYS */;
INSERT INTO `staffdetails` VALUES (38,'Sanjay Kumar','38','82374982374','sjay@hotmail.com','4983984 34 54398 349 59 398 53498 59843 59843 98543 985 435 398 543','B.B.A.','H.O.D.','10 years','','2007','CIVIL'),(343,'Harsh','343','3984789375','harshp@gmail.com','dksnfkds nfjdsn fkj ndskjf dskj fnk n','B.TECH.','ASSOCIATE PROF.','10','','22-07-1998','CE');
/*!40000 ALTER TABLE `staffdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `studentid` varchar(255) NOT NULL,
  `studentname` varchar(255) NOT NULL,
  `fathername` varchar(255) NOT NULL,
  `year` int(1) NOT NULL,
  `semester` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (1,'10091a1217','chandana','abcd',2,1),(2,'10091a1205','amitha','abcd',2,1),(3,'10091a1204','amith','xyz',2,2),(4,'10091a1212','basaiah','xyz',2,2),(5,'10091a1222','jyothirmayee','hij',3,1),(6,'10091a1262','pujitha','xyz',3,1);
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `studentdetails`
--

DROP TABLE IF EXISTS `studentdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `studentdetails` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `studentname` varchar(255) NOT NULL DEFAULT '',
  `studentid` varchar(255) NOT NULL DEFAULT '',
  `mobileno` varchar(255) NOT NULL DEFAULT '',
  `fathermobileno` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  `imagepath` varchar(255) NOT NULL DEFAULT '',
  `yearofjoining` varchar(255) NOT NULL DEFAULT '',
  `coursename` varchar(255) NOT NULL DEFAULT '',
  `department` varchar(255) NOT NULL DEFAULT '',
  `year` varchar(255) NOT NULL DEFAULT '',
  `semester` varchar(255) NOT NULL,
  `section` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `studentdetails`
--

LOCK TABLES `studentdetails` WRITE;
/*!40000 ALTER TABLE `studentdetails` DISABLE KEYS */;
INSERT INTO `studentdetails` VALUES (3,'peeyush','435','1234567890','1234567392','ac@gmail.com','dknfldsfds\r\nsdfdslfnldksfdsf','','2012','B.Tech','CSE','2017','8','B'),(6,'harshit','123','3857454398','3847983534','hsit@gmail.com','dsfdksj fkjds fkds fkjsd fkj nsdkjf dskjf nkj v','','2014','B.Tech','CIVIL','2015','2','B'),(7,'ajay','234','32894823','83458933','asdifsdo@gmail.com','kjdhnsfnds kds hfsd fjksd fkj sh','','2014','CE','CE','2017','6','C');
/*!40000 ALTER TABLE `studentdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subjectdetails`
--

DROP TABLE IF EXISTS `subjectdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subjectdetails` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `coursename` varchar(255) NOT NULL DEFAULT '',
  `branchname` varchar(255) NOT NULL DEFAULT '',
  `year` varchar(255) NOT NULL DEFAULT '',
  `semester` varchar(255) NOT NULL DEFAULT '',
  `subjectname` varchar(255) NOT NULL DEFAULT '',
  `subjectid` varchar(255) NOT NULL DEFAULT '',
  `staticid` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subjectdetails`
--

LOCK TABLES `subjectdetails` WRITE;
/*!40000 ALTER TABLE `subjectdetails` DISABLE KEYS */;
INSERT INTO `subjectdetails` VALUES (1,'B.tech','IT','2','1','Information technology fundamentals','A1201','1'),(2,'B.tech','IT','2','2','Foundations of software engineering','A1206','2'),(3,'B.tech','IT','3','1','Computer networking','A1240','3'),(4,'B.Tech','CSE','2016','7','Combinatorial Algorithms','234','2442'),(5,'B.Tech','CIVIL','2015','2','Basic Civil Eng.','23','35');
/*!40000 ALTER TABLE `subjectdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subjectplusstaff`
--

DROP TABLE IF EXISTS `subjectplusstaff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subjectplusstaff` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `staffid` varchar(255) NOT NULL DEFAULT '',
  `courseid` varchar(255) NOT NULL DEFAULT '',
  `branchid` varchar(255) NOT NULL DEFAULT '',
  `year` varchar(255) NOT NULL DEFAULT '',
  `semester` varchar(255) NOT NULL DEFAULT '',
  `section` varchar(255) NOT NULL DEFAULT '',
  `subjectid` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subjectplusstaff`
--

LOCK TABLES `subjectplusstaff` WRITE;
/*!40000 ALTER TABLE `subjectplusstaff` DISABLE KEYS */;
INSERT INTO `subjectplusstaff` VALUES (1,'343','02','02','2012','7','A','234'),(2,'38','91a','CIVIL','2016','2','B','23');
/*!40000 ALTER TABLE `subjectplusstaff` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-19 11:30:00
