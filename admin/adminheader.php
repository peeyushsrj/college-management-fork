<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Admin</title>
    <script type="text/javascript" src="../include/js/jquery-1.4.4.min.js"></script>
    <link rel="stylesheet" href="../include/css/rgit-styles.css" type="text/css" media="screen" /> 
    <style type="text/css">
        body
        {
            font-size: 12px;
            font-family: Arial;
        }
    </style>
</head>

<body>

<div class="pageContent">
	<div class="headerContent">
		<div class="logoTitle">
			<div class="logoSection">
				<img src="../images/logo.jpg" border="0"/>
			</div>
			<div class="titleSection">
				<!-- <p class="collegeName">NRI Institute of Research & Technology , Bhopal</p> -->
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="menuSection">
			<?php
			if(isset($_SESSION['username']))
				echo "<li><a href='../logout.php'>Logout</a></li>";
			else
				echo "<li><a href='../login.php'>Login</a></li>";
			?>
		</div>
	</div>
</div>
<div id="footer" style="background-color:#FFA500;clear:both;text-align:center;">
&nbsp;
</div>

<div class="adminPageContent">
	<div class='adminLeftMenu'></div>
	<div class='adminRighSection'></div>
</div>
