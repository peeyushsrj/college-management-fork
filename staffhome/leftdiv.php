<script type="text/javascript" src="../include/js/jquery-1.4.4.min.js"></script>
<script language="javascript">
$(document).ready(function () {
  $('#nav > li > a').click(function(){
    if ($(this).attr('class') != 'active'){
      $('#nav li ul').slideUp();
      $(this).next().slideToggle();
      $('#nav li a').removeClass('active');
      $(this).addClass('active');
    }
  });
});
</script>
<style type="text/css">
#nav {
    float: left;
    width: 280px;
    border-top: 1px solid #999;
    border-right: 1px solid #999;
    border-left: 1px solid #999;
}
#nav li a {
    display: block;
    padding: 10px 15px;
    background: #ccc;
    border-top: 1px solid #eee;
    border-bottom: 1px solid #999;
    text-decoration: none;
    color: #000;
}
#nav li a:hover, #nav li a.active {
    background: #999;
    color: #fff;
}
#nav li ul {
    display: none; 
}
#nav li ul li a {
    padding: 10px 25px;
    background: #ececec;
    border-bottom: 1px dotted #ccc;
}
li{
	
	list-style: none;
}
#nav{
	padding-left: 0;
	margin-left: 0;
	width: 250px;
}
</style>
<div id="menu" style="background-color:#FFD700;width:20%; height:100%;float:left;">
<ul id="nav">
	
<li><a href="profile.php">Profile</a></li>
<li><a href="attendance.php">Add attendance</a></li>
</ul>
</div>
<div id="content" style="background-color:#EEEEEE;height:100%;width:80%;float:left;">