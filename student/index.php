<?php
session_start();
include('../auth.php');
include('../admin/adminheader.php');
include('../admin/leftdiv.php');
?>
<a href="add.php">Add entry</a><br><br>
<table border="1" cellpadding="3" cellspacing="0">
<?php
include("./connect.php");
$result = mysqli_query($con, "SELECT * FROM studentdetails");
$num = mysqli_num_rows($result);
if ($num >0) {
	while($row = mysqli_fetch_assoc($result)) {
   		$studentid = $row['studentid'];
   		$studentname = $row['studentname']; 
   		echo '<tr><td><a href="update.php?studentid='.$studentid.'">'.$studentname.'</a></td><td><a href="delete.php?studentid='.$studentid.'">Delete</a></td></tr>';
	}
} else { 
	echo '<tr><td colspan="2" align="center">Nothing found</td></tr>'; 
}
mysqli_close($con);
?>
</table>
<?php include('../include/footer.php'); ?>