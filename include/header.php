<?php ob_start();
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>College manamgment System</title>
    <script type="text/javascript" src="include/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="include/js/jqsimplemenu.js"></script>
    <link rel="stylesheet" href="include/css/jqsimplemenu.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="include/css/rgit-styles.css" type="text/css" media="screen" />
    <script type="text/javascript">
        $(document).ready(function () {
            $('.rgit_menu').jqsimplemenu();
        });
    </script>
    <style type="text/css">
        body
        {
            font-size: 12px;
            font-family: Arial;
        }
    </style>
</head>

<body>

<div class="pageContent">
	<div class="headerContent"> 
		<div class="logoTitle">
			<div class="logoSection">
				<img src="images/logo.jpg" border="0"/>
			</div>
			<div class="titleSection">
				<p class="collegeName">NRI Institute of Research & Technology , Bhopal</p>
			</div>
		
		</div>
		<div class="clearfix"></div>
		<div class="menuSection">
			<ul class="rgit_menu">
				<li><a href="./index.php">Home</a></li>
				<li><a href="#">Departments </a>
					<ul>
						<li><a href="#">ECE</a></li>
						<li><a href="#">EEE</a></li>
						<li><a href="#">EIE</a></li>
						<li><a href="#">CSE</a></li>
						<li><a href="#">IT</a></li>
						<li><a href="#">MECHANICAL</a></li>
						<li><a href="#">CIVIL</a></li>
					</ul>
				</li>
				<li><a href="trainingandplacement.php">TRAINING AND PLACEMENT</a></li>
				<li><a href="library.php">LIBRARY</a></li>
				<li><a href="achievements.php">ACHIEVEMENTS</a></li>
				<li><a href="gallery.php">Gallery</a></li>
				<li><a href="hostel.php">HOSTEL</a></li>
				<li><a href="sports.php">SPORTS</a></li>
				<li><a href="aboutus.php">ABOUT US</a></li>
				<li><a href="contactus.php">CONTACT</a></li>
<?php				
if(isset($_SESSION['username']))
				echo "<li><a href='logout.php'>Logout</a></li>";
else				
				echo "<li><a href='login.php'>Login</a></li>";
?>				
			</ul>		
		</div>
	
	</div>
</div>
<div id="footer" style="background-color:#FFA500;clear:both;text-align:center;">
&nbsp;
</div>
